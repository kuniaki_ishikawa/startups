//
//  DetailMeViewController.m
//  StartUpSNS
//
//  Created by 石河邦明 on 2014/04/28.
//  Copyright (c) 2014年 k.ishikawa. All rights reserved.
//

#import "DetailMeViewController.h"

@interface DetailMeViewController ()

@end

@implementation DetailMeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
