//
//  main.m
//  StartUpSNS
//
//  Created by 石河邦明 on 2014/04/27.
//  Copyright (c) 2014年 k.ishikawa. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
